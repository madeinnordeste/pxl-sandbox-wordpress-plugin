<?php

/**
 * Plugin Name: Pxl-Sandbox-Wordpress-Plugin
 * Plugin URI:  https://communicatti.com
 * Text Domain: pxl-sandbox-wordpress-plugin
 * Description: Tools for improvement Wordpress development C
 * Author:      Communicatti
 * Author URI:  http://communicatti.com
 * Version:     0.0.7
 *
 *
* @package pxl-sandbox-wordpress-plugin
* @author  Luiz Alberto  <luizalberto@communicatti.com>
* @version 2020-07-24
*/

require_once __DIR__.'/plugin-update-checker/plugin-update-checker.php';


function pxl_sandbox_wordpress_plugin_checker_setting() {
	if ( ! is_admin() || ! class_exists( 'Puc_v4_Factory' ) ) {
		return;
	}

	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/madeinnordeste/pxl-sandbox-wordpress-plugin/',
		__FILE__,
		'pxl-sandbox-wordpress-plugin'
	);
	
	// (Opcional) If you're using a private repository, specify the access token like this:
	//$myUpdateChecker->setAuthentication('your-token-here');

	// (Opcional) Set the branch that contains the stable release.
	//$myUpdateChecker->setBranch('stable-branch-name');
}

add_action( 'admin_init', 'pxl_sandbox_wordpress_plugin_checker_setting' );


/* add more codes 5 */

/* https://gitlab.com/api/v4/projects/madeinnordeste%2Fpxl-sandbox-wordpress-plugin/repository/archive.zip?sha=v0.0.5 */